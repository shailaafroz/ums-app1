<?php include_once '../views/layout/header_view.php'; ?>

<!-- Main Section -->
	<section class="row main-content">
		<div class="col-md-12">
			<div class="dashboard-content">
				<h1>Welcome to UMS Dashboard</h1>
				<hr>
				<h2>Application based on PHP (Object-oriented) & MYSQLi with MySQL Database.</h2>
				<p>User Management System (UMS) is a web application training project.</p>
			</div>
		</div>
	</section>

<?php include_once '../views/layout/footer_view.php'; ?>